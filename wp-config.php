<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'helpnepaldb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD','direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ZRot:,4N=uT96nBM--X5g)w9,r.@eB4wl`](53MK?<UH@Q$M8:>u}rW66k<8%*l%');
define('SECURE_AUTH_KEY',  '-g8pzMy^&fRu4i|sr14mK/G!CR,Ivo)$cuH`<|lP? ,HC~DG0y#?l{WMx4@%$R{i');
define('LOGGED_IN_KEY',    '^v+1i;oIs$L#y`tFATKp7D ~z/QZYstlDI4TFT]W-wF[wK5)Qns_OJoiAD7J qgU');
define('NONCE_KEY',        'a6#BS`Ij}4XFqoK%!30`QX|U@IBFfS?LFlXvfHM}DIf_@,(!?n1Vfu:>Pgs67:[j');
define('AUTH_SALT',        'A cnW}+0Y]I~o^2JP:+gO<qVu @1@s;!RIp/SCOW*d{bJP_wRg;Vw!W+zH>,,->y');
define('SECURE_AUTH_SALT', ',s0f>k,$nil`G,:/d.1&4i>:&XIZU!7+Td6,@!V 7b-z72;F*W#aKet)!]Z| eGO');
define('LOGGED_IN_SALT',   ']WoDmCe#*],!_lA{YCs*>Mx=H@p;I{}s.cq6%J5-`f?#u)l)o>G_%ZP%ZGuXR}K^');
define('NONCE_SALT',       '1?~Jmn[_1NWybzeP}DwJ-h+ds@ZG5_#]#>gfgLa-$wZ{oE9C)`/~fUlaT&6eqzvG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

